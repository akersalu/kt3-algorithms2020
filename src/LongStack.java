import java.util.LinkedList;

/***
 * Exercise description:
 */

public class LongStack {

   public static void main (String[] argum) {

      LongStack uusStack = new LongStack();

      uusStack.push(5);
      uusStack.push(8);
      uusStack.push(-4);

      System.out.println(uusStack);

      String s = "\t2 \t5 +   \t";
      String s1 = "   \t \t356  \t \t";
      String a = "2 5 SWAP -";
      String b = " 5 9 6 ROT - +";
      String c = "2 5 9 ROT + SWAP -";
      String uuf = "2 5 1 - SWAP - 3 +";
      //TESTING THE ERROR HANDLING
      String d = "2 5 SWAP - x";
      String e = "2 5 6 SWAP - 2 + +";
      String f = "35. 10. + -";
      String g = "- 3. 4. 6. -";

      System.out.println(LongStack.interpret(s));
      System.out.println(LongStack.interpret(s1));
      System.out.println(LongStack.interpret(b));
      System.out.println(LongStack.interpret(c));
      System.out.println(LongStack.interpret(uuf));

   }

   private LinkedList<Long> list;


   LongStack() {
      this.list = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clone = new LongStack();

      int itemsAmount = this.list.size(); // actual int size not index pos of last
      while(itemsAmount > 0){

         long longToCopyOver = this.list.get(itemsAmount - 1); //get needs index pos so we use - 1
         clone.push(longToCopyOver);
         itemsAmount--;
      }
      return clone;
   }

   public boolean stEmpty() {
      return (this.list.size() == 0);
   }

   public void push (long a) {
      this.list.push(a);
   }

   public long pop() {
      try{
         return this.list.pop();
      } catch(RuntimeException e){
         throw new RuntimeException("No items left to pop from the stack(the stack is empty), underflow.");
      }
   } // pop

   public void op (String s) {
      if (this.list.size() < 2){
         throw new RuntimeException("Not enough numeric values in the stack to operate on!");
      }
      long op2 = this.list.pop();
      long op1 = this.list.pop();

      if(s.equals("+"))  {
         this.list.push(op1 + op2);
      } else if(s.equals("-")){
         this.list.push(op1 - op2);
      }else if(s.equals("*")){
         this.list.push(op1 * op2);
      }else if(s.equals("/")){
         this.list.push(op1 / op2);
      }
      else{
         throw new RuntimeException("Wrong operator, user input: " +  s);
      }
   }
  
   public long tos() {
      if (this.list.size() > 0 ){
         return this.list.peek();
      } else {
         throw new RuntimeException("Stack is empty!");
      }
   }

   @Override
   public boolean equals (Object o) {
      int stackSize = this.list.size(); //actual size again

      if (this == o) return true; //if they refer to the same object
      if (o == null) return false; //if argument is null, return false
      if (this.getClass() != o.getClass()) return false; //if the comparable classes are of different types, return false

      LongStack other = (LongStack) o; //typecast the incoming object to LongStack

      if(this.list.size() != other.list.size()) return false; //if one object is bigger than another

      long originalLong;
      long otherLong;

      while (stackSize > 0) {
         //somehow putting these two to if statement changes the outcome, making the results incorrect
         //that is why I keep them here
         originalLong = this.list.get(stackSize - 1); // here we need index pos
         otherLong = other.list.get(stackSize - 1);
         if(originalLong != otherLong) return false;
         stackSize--;
      }
      return true; //if all above is passed then objects have the same content inside but are separate objects
   }

   @Override
   public String toString() {
      int stackSize = this.list.size(); //list.size() method starts counting from 1
      if(this.list.size() < 1) return "Stack is empty.";
      StringBuffer b = new StringBuffer();
      while (stackSize > 0){
         b.append((list.get(stackSize-1)) + " "); //stackSize - 1 to get the correct index(last in stack)
         stackSize--;
      }
      return b.toString();
   }

   public static long interpret (String pol) {
      if (pol == null || pol.isEmpty()) throw new RuntimeException("The input field is empty");
      // Trim and clean the string from redundant spaces
      String[] polArr = pol.trim().split(" +");


      //We create a list from the array
      LinkedList<String> cleanList = new LinkedList();

      // selle for tsükliga käime algusest lõppu üle kõik elemendid polArr-is
      for (int i = 0; i < polArr.length; i++) {
         // in a certain case with \t (escape sequence (tab)) the string still does not get cleaned so we trim each array slot
         polArr[i] = polArr[i].trim();
         //Check for illegal arguments and if valid then push to cleanList(which is a linked list holding numbers and letters).
         if (isNumeric(polArr[i]) || isOperator(polArr[i])) {
            cleanList.addLast(polArr[i]);
         } else {
            throw new RuntimeException("Incorrect arguments given by user.\n User input: " + pol +
                    " Detected wrong argument: " + polArr[i]);
         }
      }

      //We create a new stack to push items we currently process into
      LongStack stackValues = new LongStack();

      //Stack balance ja stack underflow kontroll
      if ( cleanList.size() > 1 && isNumeric(cleanList.peekLast()) ){
         throw new RuntimeException("Last input value should not be an operand, it sould be an operator.\n" +
                 " User input: " + pol + " Last input value: " + cleanList.peekLast());
      }

      while(cleanList.size() > 0){
         String poppedVal = cleanList.pop();
         //SWAP ÜLESANNE
         if (poppedVal.equals("SWAP") && stackValues.list.size() > 1){

            long op1 = stackValues.pop();
            long op2 = stackValues.pop();
            stackValues.push(op1);
            stackValues.push(op2);

         } else if (poppedVal.equals("ROT")){ //ROT ÜL
            try{
               long op1 = stackValues.list.get(2);
               stackValues.list.remove(2);
               stackValues.list.push(op1);
            } catch (RuntimeException e){
               throw new RuntimeException("Cannot rotate less than three operands, current user operands " +
                       "for ROT operation: " + stackValues.toString() + " \n Whole user input string: " + pol);
            }

         } else if (isNumeric(poppedVal)){ //if the current argument is an operand
            stackValues.push(Long.parseLong(poppedVal));
            //if the current argumet is an operator
         } else {
            //try to run the operation with the given operator if it fails then we have underflow
            try{
               stackValues.op(poppedVal);
            } catch (RuntimeException e){
               //e.printStackTrace();
               throw new RuntimeException("User input caused underflow and thus is incorrect.\n " +
                       "Check your position and balance of operands and operators. User input: " + pol);
            }
         }
      }

      //Lastly if there is more than one item left in the stack, throw an error
      if(stackValues.list.size() > 1) throw new RuntimeException("Too many items left in the stack, " +
              "there should be only one item. User input: " + pol);


      return stackValues.pop();
   }

   public static boolean isNumeric (String str) {
      try{
         long d = Long.parseLong(str);
      } catch (NumberFormatException | NullPointerException nfe ){
         return false;
      }
      return true;
   }

   public static boolean isOperator(String str){
      if (str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/")
              || str.equals("SWAP") || str.equals("ROT")) {
         return true;
      } else return false;
   }

}

